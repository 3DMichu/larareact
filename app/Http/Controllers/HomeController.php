<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function index(){
        if(!Auth::check()){
            return redirect('login');
        }
        $messages = \App\Message::all();

        return view('home', [
            'messages' => $messages
        ]);
    }

    public function registerGet(){
        return view('register');
    }

    public function register(Request $request){
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        $user = new \App\User();
        $user->name = $request->username;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect('/');
    }

    public function loginGet(){
        return view('login');
    }

    public function login(Request $request){
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        Auth::attempt(['email' => $request->email, 'password' => $request->password]);

        return redirect('/');
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect('/login');
    }
}
