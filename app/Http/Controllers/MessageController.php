<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\UsersMessage;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    public function create(Request $request){
        $user = Auth::user();
        $message = new Message();
        $message->person = $user->name;
        $message->title = $request->title;
        $message->content = $request->content;
 
        $message->save();

        $userMessage = new \App\UsersMessage;
        $userMessage->user_id = $user->id;
        $userMessage->message_id = $message->id;

        $userMessage->save();

        return redirect('/');
    }

    public function view($id){
        $message = Message::findOrFail($id);

        return view('message', [
            'message' => $message
        ]);
    }

    public function allMessages(){
        $messages = Message::all();
        return view('messageall', ['messages'=>$messages]);
    }

    public function removeMessage($message_id){
        DB::table('messages')->where('id', $message_id)->delete();
        return redirect('/');
    }

    public function allUsers(){
        return view('usersStats');
    }

    public function allUsersData(){
        $usersMessages = UsersMessage::all();
        $data_array = [];
        $i = 0;
        $userEX = "";
        foreach($usersMessages as $uMessage){
            $id_user = $uMessage->user_id;
            $id_message = $uMessage->message_id;

            $user = DB::table('users')->where('id', $id_user)->first();
            $message = DB::table('messages')->where('id', $id_message)->first();

            $user_name = $user->name;
            if($i == 0){
                $userEX = $user_name;
            }else{
                if($userEX != $user_name){
                    $i = 0;
                    $userEX = $user_name;
                }
            }

            $message_title = $message->title;
            $message_content = $message->content;
            $message_created_at = $message->created_at;

            $data_array[$user_name]['messages'][$i]['title'] = $message_title;
            $data_array[$user_name]['messages'][$i]['content'] = $message_content;
            $data_array[$user_name]['messages'][$i]['created_at'] = $message_created_at;
            $i=$i+1;
        }
        //->response(json_encode($data_array[0][0]), 200);
        return response(json_encode($data_array), 200);
    }
}
