<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::post('/create', 'MessageController@create');
Route::get('/deleteMessage/{message_id}', 'MessageController@removeMessage');
Route::get('/message/{id}', 'MessageController@view');
Route::get('/messages', 'MessageController@allMessages');

Route::get('/register', 'HomeController@registerGet');
Route::post('/register', 'HomeController@register');
Route::get('/login', 'HomeController@loginGet');
Route::post('/login', 'HomeController@login');
Route::get('/logout', 'HomeController@logout');

Route::get('/users', 'MessageController@allUsers');
Route::get('/usersData', 'MessageController@allUsersData');