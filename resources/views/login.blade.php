@extends('master')

@section('title', 'Logging in')

@section('content')
        <form action="/login" method="post">
            <div className="registerInput">
                Email<input type="text" name="email"></input>
                Password<input type="password" name="password" placeholder="Password required" autocomplete="off"></input>
                @csrf
                <button type="submit">Login</button>
            </div>
        </form>
@endsection