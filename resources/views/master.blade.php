<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        .TableReact{
            margin-top: 25px;
        }
        th, td {
            padding: 5px;
            text-align: left;
        }
    </style>
</head>
<body>

    @yield('content')
    <!--<script src="./js/app.js"></script>!-->

</body>
</html>