@extends('master')

@section('title', 'Homepage')

@section('content')

    Post a message:

    <form action="/create" method="post">
        <input type="text" name="title" placeholder="Title">
        <input type="text" name="content" placeholder="Content">
        {{ csrf_field() }}
        <button type="submit">Submit</button>
    </form>

    <br>

    Recent Messages:

    <ul>
        @forelse($messages as $message)
            <li>
                <strong>{{$message->title}} by {{$message->person}}</strong>
                <br>
                {{$message->content}}
                <br>
                {{$message->created_at->format('d/m/Y H:i')}}
                <br>
                {{$message->created_at->diffForHumans()}}
                <br>
                <a href="/message/{{$message->id}}">View</a>
                <br>
                <button onclick="location.href='{{ url('/deleteMessage/'.$message->getKey()) }}'">Delete Message</button>
            </li>
            @empty
                <p>No messages currently.</p>
        @endforelse
    </ul>

    <button onclick="location.href='{{ url('/logout/') }}'">Logout</button>

@endsection