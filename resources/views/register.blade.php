@extends('master')

@section('title', 'Registration')

@section('content')
        <form action="/register" method="post">
            <div className="registerInput">
                Username<input type="text" name="username" placeholder="Username required" autocomplete="off"></input>
                E-mail<input type="text" name="email"></input>
                Password<input type="password" name="password" placeholder="Password required" autocomplete="off"></input>
                @csrf
                <button type="submit">Register</button>
            </div>
        </form>
@endsection