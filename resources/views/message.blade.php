@extends('master')

@section('title', $message->title)

@section('content')

    <h3>{{$message->title}}</h3>
    <h2>{{$message->content}}</h2>

@endsection