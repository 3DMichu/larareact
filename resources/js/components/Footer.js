import React, { Component } from "react";

class Footer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: "Default text"
        };
    }

    changeFooter() {
        this.setState(
            {
                text: "Dzięki mordo, działa"
            },
            () => {
                console.log("callback - changeFooter()");
            } //!Don't!!!!!!!!!!!
        );
    }

    render() {
        return (
            <div>
                <button onClick={() => this.changeFooter()}>
                    {this.state.text}
                </button>
            </div>
        );
    }
}

export default Footer;
