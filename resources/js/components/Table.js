import React, { Component } from "react";

const Table = ({ caption, header1, header2, header3 }) => {
    return (
        <div className="TableReact">
            <table>
                <caption>{caption}</caption>
                <tbody>
                    <tr>
                        <th>{header1}</th>
                        <th>{header2}</th>
                        <th>{header3}</th>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

export default Table;
