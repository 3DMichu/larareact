import React from "react";

class Header extends React.Component {
    constructor() {
        super();
        this.state = {
            text: "Header Component Default Message"
        };
    }

    changeMessage() {
        fetch("http://localhost:8000/messagejson/4", {
            method: "get"
        })
            .then(resp => resp.text())
            .then(response => {
                this.setState({
                    text: response.split("&")[1]
                });
            });
    }

    render() {
        return (
            <div className="container">
                <button onClick={() => this.changeMessage()}>
                    {this.state.text}
                </button>
            </div>
        );
    }
}

export default Header;
