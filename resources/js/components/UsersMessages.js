import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from "axios";

class UsersMessages extends Component {
    constructor() {
        super();
        this.state = {
            users: []
        };
    }

    componentDidMount() {
        axios
            .get("/usersData")
            .then(resp => resp.data)
            .then(response => {
                this.setState({ users: response });
            });
    }

    render() {
        console.log(this.state.users);
        const list = Object.keys(this.state.users).map(userKey => {
            return (
                <li key={userKey}>
                    {userKey}
                    <ul>
                        {this.state.users[userKey]["messages"].map(
                            messageKey => {
                                return (
                                    <li key={messageKey.title}>
                                        Title:
                                        <strong>{messageKey.title}</strong>
                                        <ul>
                                            <li key="date">
                                                Created at:<br></br>
                                                {messageKey.created_at}
                                            </li>
                                            <li key="content">
                                                Content:<br></br>
                                                {messageKey.content}
                                            </li>
                                        </ul>
                                    </li>
                                );
                            }
                        )}
                    </ul>
                </li>
            );
        });
        return <ul>{list}</ul>;
    }
}

if (document.getElementById("example")) {
    ReactDOM.render(<UsersMessages />, document.getElementById("example"));
}

export default UsersMessages;
