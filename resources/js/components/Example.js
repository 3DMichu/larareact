import React from "react";
import ReactDOM from "react-dom";
import Header from "./Header";
import Footer from "./Footer";
import Table from "./Table";

function Example() {
    return (
        <div className="container">
            <Header></Header>
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <div className="card-header">Example Component</div>

                        <div className="card-body">
                            I'm an example component!
                        </div>
                    </div>
                </div>
            </div>
            <Footer></Footer>
            <Table
                caption="Test Table - React"
                header1="Header1"
                header2="Header2"
                header3="Header3"
            ></Table>
        </div>
    );
}

export default Example;

if (document.getElementById("example")) {
    ReactDOM.render(<Example />, document.getElementById("example"));
}
